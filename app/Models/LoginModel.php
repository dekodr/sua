<?php

namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model
{
    protected $table = 'ms_admin';
    // protected $allowedFields = ['title', 'img'];
    public function check($param)
    {
        $session = \Config\Services::session();

        $username = $param['username'];
        $password = $param['password'];

        $data = $this->login($username, $password);

        if (!empty($data)) {
            $set_session = array(
                'id_user'         =>    $data->id,
                'id_role'         =>    $data->id_role,
                'name'            =>    $data->name,
                'email'           =>    $data->email,
                'id_tutor'        =>    $data->id_tutor
            );
            session()->set('admin', $set_session);
            return true;
        } else {
            return false;
        }
    }

    function login($username, $password)
    {
        $options = [
            'baseURI' => 'http://103.93.56.248:8081',
            'timeout'  => 3
        ];
        $client = \Config\Services::curlrequest($options);

        $response = $client->request('POST', '/login_admin', [
            'form_params' => [
                'username' => $username,
                'password' => $password
            ]
        ]);

        $r = json_decode($response->getBody());

        return $r->data;
    }
}
