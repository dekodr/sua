<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'ms_user';
    protected $allowedFields = ['id', 'name', 'email', 'number_id', 'division', 'position', 'photo_profile', 'photo_id', 'entry_stamp', 'edit_stamp', 'del'];


    public function get_user($id=null){
        if($id !== null){
            
            $data = $this->where(['number_id' => $id])->get()->getRow();
            return $data;
        }else{
            $data = $this->where(['del' => 0])->get()->getResult();
            return ($data);
        }
    }

    public function insert_user($_param){
        $_param['entry_stamp']  = date("Y-m-d h:i:s");
        
        //saving data
        $this->insert($_param);
        return TRUE;
    }

    public function get_participant(){
        $data = $this->where(['del' => 0])->get()->getResult();

        //Getting User Participant
        foreach ($data as $key => $value)
            $participant[] = $value->name." (".$value->number_id.")";

        $tags = array();
        foreach ($data as $key => $value) {
            $split = explode(" ",$value->division);
            foreach ($split as $split_) {
                array_push($tags, $split_);
            }
        }

        $tags   = array_unique($tags);
        $return = array_merge($participant, $tags);
        
        return ($return);
    }
}
