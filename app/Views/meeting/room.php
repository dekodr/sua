<html itemscope itemtype="http://schema.org/Product" prefix="og: http://ogp.me/ns#" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html;charset=utf-8">
        <style>
            * {
                padding: 0;
                margin: 0;
            }
        </style>
    </head>
    <body>
        <script src="http://polaapp.com/external_api.js"></script>
        <script>
            var domain = "polaapp.com/";
            var options = {
                roomName: '<?php echo $meeting[0]->name;?>', //can create before start
                width: "100%",
                height: "100%",
                userInfo: {
                    email: '<?php echo $user->name;?>', //set email optional (when registration)
                    displayName: '<?php echo $user->email;?>' //must set displayName 
                },
                parentNode: undefined,
                configOverwrite: {},
                // interfaceConfigOverwrite: {
                    // SHOW_POWERED_BY: false,
                    // SHOW_WATERMARK_FOR_GUESTS: false,
                    // SHOW_PROMOTIONAL_CLOSE_PAGE: false,
                    // SHOW_JITSI_WATERMARK: false,
                    // SHOW_PROMOTIONAL_CLOSE_PAGE: true
                // },
                prejoinPageEnabled: false
            }
            var api = new JitsiMeetExternalAPI(domain, options);
        </script>
    </body>
</html>