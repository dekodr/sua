<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
    <!-- Page Heading -->
    
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">User</h1>
	</div>

	<?php if(session()->getFlashdata('error')){?>
		<div class="card mb-4 py-3 border-left-danger">
			<div class="card-body">
				<?php print_r(session()->getFlashdata('error'))?>
			</div>
		</div>
	<?php }?>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Form - Create User</h6>
		</div>
		<div class="card-body row">
			<div class="col-xl-8 col-lg-7">
				<form method="POST" class="user" accept-charset="utf-8" enctype="multipart/form-data">
					
					<div class="form-group">
						<input name="name" type="text" class="form-control form-control-user" id="name" aria-describedby="emailHelp" placeholder="Full Name" required>
					</div>
					<div class="form-group">
						<input name="number_id" type="text" class="form-control form-control-user" id="number_id" aria-describedby="emailHelp" placeholder="No. ID User" required>
					</div>
					<div class="form-group">
						<input name="email" type="email" class="form-control form-control-user" id="email" aria-describedby="emailHelp" placeholder="Email Address" required>
					</div>
					<div class="form-group row">
						<div class="col-sm-6 mb-3 mb-sm-0">
							<input name="position" type="text" class="form-control form-control-user" id="position" placeholder="Position" required>
						</div>
						<div class="col-sm-6">
							<input name="division" type="text" class="form-control form-control-user" id="division" placeholder="Division" required>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6 mb-3 mb-sm-0">
							<input name="photo_id" type="file" class="btn btn-secondary btn-icon-split" id="photo_id" aria-describedby="emailHelp" placeholder="Profile Picture" required>
						</div>
					</div>
					
					<hr>
					<button type="submit" class="btn btn-primary btn-icon-split">
						<span class="icon text-white-50">
						<i class="fas fa-save"></i>
						</span>
						<span class="text">Save Data</span>
					</button>
				</form>
			</div>
			
			<div class="col-xl-4 col-lg-5">
				<img style="width: 100%" src="<?php echo base_url('assets/img/bg-create-user.png')?>">
			</div>
		</div>
    </div>
<?php echo $this->endSection() ?> ?>
<?php echo $this->section('script') ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#division").hashtags();
		});
	</script>
<?php echo $this->endSection() ?> ?>
         