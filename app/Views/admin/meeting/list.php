<?php echo $this->extend('base/template'); ?>

<?php echo $this->section('content') ?>
    <!-- Page Heading -->
    
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Meeting Schedule</h1>
        <a href="<?php echo base_url('admin/meetings/create') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Create New Meeting</a>
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Meeting Schedule List</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Meeting Name</th>
                <th>Date</th>
                <th>Start Time</th>
                <th>End Time</th>
                <!-- <th>Participant</th> -->
                <th>Action</th>
            </tr>
            </thead>    
            <tfoot>
            <tr>
                <th>Meeting Name</th>
                <th>Date</th>
                <th>Start Time</th>
                <th>End Time</th>
                <!-- <th>Participant</th> -->
                <th>Action</th>
            </tr>
            </tfoot>
            <tbody>
                <?php echo($table);?>
            </tbody>
        </table>
        </div>
    </div>
    </div>

<?php echo $this->endSection() ?> ?>

<?php echo $this->section('script') ?>
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });
    </script>
<?php echo $this->endSection() ?> ?>
         
         