<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Sua.</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="icon" href="<?php echo base_url('assets/sua-login/images/sua-icon.png')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/sua-login/styles/main.css')?>" media="all"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>  
		<script src="<?php echo base_url('assets/sua-login/js/js-master.js')?>"></script>
    
        <!-- Facerecog Level -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.css"> -->
        <script src="<?php echo base_url('assets/face-api/face-api.js')?>"></script>
        <script src="<?php echo base_url('assets/face-api/js/commons.js')?>"></script>
        <script src="<?php echo base_url('assets/face-api/js/faceDetectionControls.js')?>"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

        <style>
            .camera-box{
                overflow:hidden;
            }
            video{
                height: 100%;
                transform: rotateY(180deg);
                -webkit-transform:rotateY(180deg); /* Safari and Chrome */
                -moz-transform:rotateY(180deg); /* Firefox */
            }
            canvas{
                display:none;
                position: absolute;
                top: 0;
                left: 0;
            }
        </style>
    </head>
	<body>
		<div class="master-body">
            <div class="login-screen section-1">
                <div class="section-left">
                    <div class="box-center">
                        <div class="content">
                            <img src="<?php echo base_url('assets/sua-login/images/logo-2.png')?>" class="logo-master">
                            
                            <div class="hidden-frame">
                                <div class="title big">
                                    Terhubung tanpa batasan apapun.
                                </div>
                                <div class="title small">
                                    Enterprise Video Conferencing
                                </div>
                                <div class="button-frame">
                                    <div class="btn-1 create-meeting-trigger primary">
                                        Rapat Instant
                                    </div>
                                    <div class="btn-2 facebook">
                                        <img src="<?php echo base_url('assets/sua-login/images/icon-facebook.png')?>">
                                    </div>
                                    <div class="btn-2 google">
                                        <img src="<?php echo base_url('assets/sua-login/images/icon-google.png')?>">
                                    </div>
                                </div>
                            </div>    

                            <div class="create-meeting-frame">
                                <div class="input-frame-1 mt-24">
                                    <div class="title">Masukan judul rapat</div>
                                    <input type="text" id="meetingInput" class="input large" placeholder="Judul Rapat">
                                </div> 
                                <br class="clear"> 
                                <div class="btn btn-s saveMeetingInstant primary mr-24">
                                    Buat Rapat
                                </div>
                                <div class="btn btn-s outline-red cancel-meeting-trigger">
                                    Batal
                                </div>
                            </div>
                        </div>
                    </div>
                </div>     
                <div class="section-right">
                    <div class="box-left">
                        <div class="box-frame login-button">
                            <div class="btn btn-l white trigger-face" id="face-login">
                                Masuk dengan Muka
                            </div>
                            <div class="btn btn-l outline-white mt-24 trigger-email">
                                Masuk dengan Email
                            </div>
                            <div class="bottom-text">
                                Belum punya akun?
                            </div>
                        </div>
                    </div>
                    <div class="hidden-box login-face">
                        <div class="text">
                            Masuk dengan muka anda<br>
                            <span class="small">
                                Posisikan muka anda dalam frame
                            </span>
                        </div>
                        <div class="camera-box">
                            <video onloadedmetadata="onPlay(this)" id="inputVideo" autoplay muted playsinline></video>
		                    <canvas id="overlay" />
                        </div>
                        <div class="text-bottom trigger-email">
                            Masuk dengan Email & Password
                        </div>
                        <div class="overlay-back"></div>
                    </div>
                    
                    <div class="hidden-box login-userid">
                        <div class="white-box">
                            <div class="inner-padding">
                                <div class="input-frame-1 mt-24">
                                    <div class="title">Email</div>
                                    <input type="text" class="input" placeholder="Your email">
                                </div>  
                                <div class="input-frame-1">
                                    <div class="title">Password</div>
                                    <input type="password" class="input" placeholder="Your Password">
                                </div> 
                                <div class="btn btn-fl primary">
                                    Masuk
                                </div>
                            </div>
                        </div>
                        <div class="text-bottom trigger-face">
                            Masuk dengan Muka
                        </div>
                        <div class="overlay-back"></div>
                    </div>
                    <div class="accent accent-1"></div>
                </div>
                <br class="clear"/>
            </div>
        </div>
    </body>
    
    <!-- Facerecognition Script -->
    <script>
        let forwardTimes = [];
        let withBoxes = true;

        var request = null;
        var isLoading = false;
        
        
        function onChangeHideBoundingBoxes(e) {
            withBoxes = !$(e.target).prop('checked')
        }

        function updateTimeStats(timeInMs) {
            forwardTimes = [timeInMs].concat(forwardTimes).slice(0, 30)
            const avgTimeInMs = forwardTimes.reduce((total, t) => total + t) / forwardTimes.length
            $('#time').val(`${Math.round(avgTimeInMs)} ms`)
            $('#fps').val(`${faceapi.utils.round(1000 / avgTimeInMs)}`)
        }

        async function onPlay() {
            const videoEl = $('#inputVideo').get(0)
            
            if(videoEl.paused || videoEl.ended || !isFaceDetectionModelLoaded())
                return setTimeout(() => onPlay())


            const options = getFaceDetectorOptions()

            const ts = Date.now()

            const result = await faceapi.detectSingleFace(videoEl, options).withFaceExpressions()

            updateTimeStats(Date.now() - ts)
            console.log(">>>",result)

            if (result) {
                const canvas = $('#overlay').get(0)
                const dims = faceapi.matchDimensions(canvas, videoEl, true)

                const resizedResult = faceapi.resizeResults(result, dims)
                const minConfidence = 0.5


                if (withBoxes) {
                    console.log("boxes display")

                    takepicture();
                    videoEl.ended;
                    faceapi.draw.drawDetections(canvas, resizedResult)
                    return setTimeout(() => onPlay())
                }
                faceapi.draw.drawFaceExpressions(canvas, resizedResult, minConfidence)
            }else{
                
            }

            setTimeout(() => onPlay())
        }

        async function run() {
            // load face detection and face expression recognition models
            await changeFaceDetector(TINY_FACE_DETECTOR)
            await faceapi.loadFaceExpressionModel('/')
            changeInputSize(320)

            // try to access users webcam and stream the images
            // to the video element
            const stream        = await navigator.mediaDevices.getUserMedia({ video: {} })
            const videoEl       = $('#inputVideo').get(0)
            videoEl.srcObject   = stream
            
        }

        function updateResults() {}

        async function stopVid(){
            console.log("stop vid")
            // $('#inputVideo')
        }
        
        async function takepicture() {

            const canvas    = $('#overlay').get(0)
            const videoEl   = $('#inputVideo').get(0)
            var context     = canvas.getContext('2d');

            context.drawImage(videoEl, 0, 0, 640, 480);

            var data    = canvas.toDataURL('image/png');
            
            // console.log("photo data in base64", data)
            if (request != null){
                request.abort();
                request = null;
            }

                request = $.ajax({
                    type    : "POST",
                    async   : true,
                    url     : '<?php echo base_url('admin/master/user/absent');?>',
                    headers : {'X-Requested-With': 'XMLHttpRequest'},
                    data    : {
                                base64Img: data
                            },
                    statusCode: {
                                404: function() {
                                alert( "page not found" );
                                }
                            }
                }).then(function(obj){
                    console.log("SUKSES BOS MANTAP")
                    obj = JSON.parse(obj);
                    console.log("nik>", obj.nik);
                    // result = obj;
                    if(obj.nik !== undefined){
                        window.location.href = "<?php echo base_url('admin/meetings/room?');?>"+"?q="+obj.nik;
                    }else{
                        window.location.href = "<?php echo base_url();?>";
                    }
                })
                // .fail(function(o){
                //     console.log("GAGAL BOS")
                //     console.log(o);
                //     // alert(o);
                //     window.location.href = "<?php #echo base_url();?>";
                // })
            
                

                
        }

        function clearphoto() {
            var context = canvas.getContext('2d');
            context.fillStyle = "#AAA";
            context.fillRect(0, 0, canvas.width, canvas.height);

            var data = canvas.toDataURL('image/png');
            photo.setAttribute('src', data);
        }

        $(document).ready(function() {
        })

        $( "#face-login" ).click(function() {
            initFaceDetectionControls()
            run()
        });
        $( ".saveMeetingInstant" ).click(function() {
            var roomName = $('#meetingInput').val();
            initMeeting(roomName);
        });

    </script>

    <script src="http://polaapp.com/external_api.js"></script>
    <script>
        function initMeeting(roomName){
            window.location.href = "<?php echo base_url('admin/meetings/instant?');?>"+"?q="+roomName;
        }
    </script>
</html>

