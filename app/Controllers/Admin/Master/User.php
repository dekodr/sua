<?php namespace App\Controllers\Admin\Master;

use App\Controllers\BaseController;
use App\Models\UserModel;
use CodeIgniter\HTTP\Files\UploadedFile;

// use Aws\S3\S3Client;
// use Aws\Exception\AwsException;
// use Aws\S3\ObjectUploader;

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

// ini_set('memory_limit', '1024M');
// ini_set('upload_max_filesize', '200000M');
class User extends BaseController
{	
    public $UserModel;
    public $generateTable;

    public function __construct()
    {
        $this->UserModel        = new UserModel();
        $this->generateTable    = new \CodeIgniter\View\Table();
    }
    
	public function index()
	{
        $data   = $this->UserModel->get_user();
        
        foreach ($data as $key => $value)
            $data['table'][$key] = array($value->name,$value->position,$value->division,$value->email);
        
        $data['table']  = $this->generateTable->generate($data['table']);

        //returning table body
        return view('admin/master/user/list', $data);
    }

	public function create()
	{   
        // $this->s3('/Users/didd/Office/project/sua/website/php/sua/writable/uploads/register/1599555364_904d09b2cbf527fc4bcc.jpeg'); 
        if ($_POST['name']) {
            # code...
            
            $file               = $this->request->getFiles('photo_id');

            // DATABASE DATA FORMAT
            $data               = $_POST;
            $data['photo_id']   = $file['photo_id']->getRandomName();
            $file['photo_id']->move(WRITEPATH.'uploads/register', $data['photo_id']); //upload to dir
            $photo_uri          = WRITEPATH.'uploads/register/'.$data['photo_id'];
            $data['photo_id']   = $this->s3($data['photo_id'], $photo_uri, "register");

            // RKB DATA FORMAT
            $rkbDATA = array();
            $rkbDATA['name']            = $data['name'];
            $rkbDATA['company_id']      = 3;
            $rkbDATA['phone_no']        = uniqid();
            $rkbDATA['email']           = $data['email'];
            $rkbDATA['nik']             = $data['number_id'];
            $rkbDATA['photo']           = $data['photo_id'];

            print_r(json_encode($rkbDATA));
            // registering face id
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-Type: application/json\r\n",
                    'content' => json_encode($rkbDATA)
                )
            ));
            $response = file_get_contents('http://103.93.56.248:8000/absent/registration', FALSE, $context);

            if($response === FALSE)
                die('Error');

            $responseData = json_decode($response, TRUE);

            //save to db
            $save               = $this->UserModel->insert_user($data);

            if ($save) {
                return redirect()->to(base_url('admin/master/user'))->with('success', 'Data successfully saved.');
            } else {
                return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
            }
        }
        
        return view('admin/master/user/create');
    }
    
    public function absent($data=null)
    {
        if ($this->request->isAJAX()) {
            $base64Img = service('request')->getPost('base64Img');
            
            //------------------------- decode image
            $dataENCODED    = $this->request->getPost('base64Img');
            $image_parts    = explode(";base64,", $dataENCODED);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type     = $image_type_aux[1];
            $image_base64   = base64_decode($image_parts[1]);
            $filename       = uniqid().'.jpg';
            $file           = WRITEPATH.'uploads/absent/'.$filename;
            $return         = file_put_contents($file, $image_base64);

            //------------------------- upload S3
            $linkS3 = $this->s3($filename, $file, 'absent');
            
            
            //------------------------- absent confirmation
            $rkbDATA = array();
            $rkbDATA['photo']       = $linkS3;
            $rkbDATA['company_id']  = 3;

            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-Type: application/json\r\n",
                    'content' => json_encode($rkbDATA)
                )
            ));

            $response = file_get_contents('http://103.93.56.248:8000/absent/confirmation', TRUE, $context);

            print_r($response);
            die;
            if ($response) {
                # code...

                print_r($file);
                print_r($linkS3);

                return json_encode($response);
            }else{
                // return "User tidak ditemukan";
                return $response;
            }
        }else{
            // die;
            $rkbDATA = array();
            $rkbDATA['photo']       = 'https://rkb-fr.s3-ap-southeast-1.amazonaws.com/absent/5f58e35be068c.jpg';
            $rkbDATA['company_id']  = 3;

            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-Type: application/json\r\n",
                    'content' => json_encode($rkbDATA)
                )
            ));
            
            $result = file_get_contents('http://103.93.56.248:8000/absent/confirmation', FALSE, $context);
            $result = json_decode($result);
            var_dump($result);

            // return "kosong gan";
        }
    }

    //saving file to s3
    public function s3($key, $uri, $dir){
        $s3 = new S3Client([
            'region'  => 'ap-southeast-1',
            'version' => 'latest',
            'credentials' => [
                'key'    => 'AKIAIFQK53A6O7EOSTVQ',
                'secret' => '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'
            ]
        ]);
        
        // Send a PutObject request and get the result object.
        $key = $dir."/".$key;
        
        $result = $s3->putObject([
            'Bucket'        => 'rkb-fr',
            'Key'           => $key,
            // 'SourceFile' => 'file:///Users/didd/Office/project/sua/website/php/sua/writable/uploads/register/1599555364_904d09b2cbf527fc4bcc.jpeg'
            'SourceFile'    => $uri,
            'ACL'           => 'public-read',

        ]);
        
        // Print the body of the result by indexing into the result object.
        // var_dump($result["@metadata"]["effectiveUri"]);

        return $result["@metadata"]["effectiveUri"];
    }
	//--------------------------------------------------------------------

}