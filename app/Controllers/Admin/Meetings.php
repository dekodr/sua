<?php namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Models\MeetingsModel;
use App\Models\UserModel;

class Meetings extends BaseController
{	
    
    public $MeetingsModel;
    public $UserModel;
    public $generateTable;

    public function __construct()
    {
        $this->MeetingsModel    = new MeetingsModel();
        $this->UserModel        = new UserModel();
        $this->generateTable    = new \CodeIgniter\View\Table();
    }
    
	public function index()
	{
        
        $data   = $this->MeetingsModel->get_meeting();
        
        foreach ($data as $key => $value)
            $data['table'][$key] = array($value->name,$value->date,$value->start_time,$value->end_time);
        
        $data['table']  = $this->generateTable->generate($data['table']);

        return view('admin/meeting/list', $data);
    }
    

	public function create()
	{
        if ($_POST['name']) {
            # code...
            $data   = $_POST;
            $save   = $this->MeetingsModel->insert_meeting($data);

            if ($save) {
                return redirect()->to(base_url('admin/meetings'))->with('success', 'Data successfully saved.');
            } else {
                return redirect()->back()->withInput()->with('error', 'We had trouble saving your data.');
            }
            
        }

        $data['participant']    = $this->UserModel->get_participant();
        return view('admin/meeting/create', $data);
    }


    public function room(){
        $number_id = $_GET['q']; // nik or number id or whatever

        $data = $this->meetingLIST($number_id);
        return view('meeting/room', $data);
    }

    public function meetingLIST($number_id){
        return $this->MeetingsModel->meetingLIST($number_id);
    }


    public function instant(){
        $data['roomName'] = $_GET['q']; // nik or number id or whatever

        return view('meeting/instant', $data);
    }
	//--------------------------------------------------------------------

}
